= Authors
:url-codeberg: https://codeberg.org

== Liblast, a libre multiplayer FPS game

Copyleft (ɔ) 2020-2022 Liblast team, Liblast contributors

=== Liblast team members
[cols=">1,^1,<1", options="header"]
|===
|Account
|Name
|Role

|{url-codeberg}/unfa[unfa^]
|Tobiasz Karoń
|Project lead, code, art, audio, merge conflict introducer

|{url-codeberg}/combustiblelemonade[CombustibleLemonade^]
|Jan Heemstra
|

|{url-codeberg}/gilgamesh[gilgamesh^]
|Adham Omran
|

|{url-codeberg}/nebunez[nebunez^]
|John Ryan
|

|{url-codeberg}/Mo8it[Mo8it^]
|Mo Bitar
|Code development, documentation

|{url-codeberg}/siina[siina^]
|Siina Mashek
|UI/UX design, coding, voice acting, documentation

|{url-codeberg}/xantulon[xantulon^]
|Xan
|3D Generalist
|===

==== Inactive team members
[cols=">1,^1,<1", options="header"]
|===
|Account
|Name
|Role

|{url-codeberg}/ardem[aRdem^]
|JM
|Concept art, character design, level design


|{url-codeberg}/Pablo_Pozo[Pablo_Pozo^]
|Pablo Pozo
|3D Artist
|===

=== Liblast contributors

[cols=">1,<1", options="header"]
|===
|Account
|Name

|YOUR ACCOUNT
|YOUR NAME
|===
