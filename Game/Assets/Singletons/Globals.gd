extends Node

### This singleton holds globalkly required enums and constants

enum DamageType {
	NONE,
	BULLET,
	EXPLOSION,
	ENVIRONMENT_HAZARD,
}

#enum MaterialType { NONE,
#					CONCRETE,
#					METAL,
#					WOOD,
#					GLASS,
#					WATER,
#					}
