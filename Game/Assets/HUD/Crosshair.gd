extends Control

@onready var aplayer = $AnimationPlayer

func hit():
	aplayer.stop()
	aplayer.play("Hit")
	$ConfirmSound.play()
	
func kill():
	aplayer.stop()
	aplayer.play("Kill")
	$ConfirmSound2.play()
