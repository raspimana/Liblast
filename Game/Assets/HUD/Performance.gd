extends RichTextLabel

@onready var main = get_tree().root.get_node("Main")

func _process(delta) -> void:
	if not get_tree().get_multiplayer().has_multiplayer_peer():
		text = "Offline"
	elif get_tree().get_multiplayer().is_server():
		text = "Hosting · "
		var peers = get_tree().get_multiplayer().get_peers().size()
		if peers == 0:
			text += "no peers"
		elif peers == 1 :
			text += "1 peer"
		else:
			text += str(peers) + " peers"
	else:
		text = "Connected · "
		#print(get_multiplayer_authority())
		var own_peer = get_tree().get_multiplayer().multiplayer_peer.get_peer(get_multiplayer_authority())
		var ping = own_peer.get_statistic(ENetPacketPeer.PEER_ROUND_TRIP_TIME)
		var packet_loss = own_peer.get_statistic(ENetPacketPeer.PEER_PACKET_LOSS)
		text += str(ping) + " ms · " + str(packet_loss)

		# propagate this info
		var local_pid = get_tree().get_multiplayer().get_unique_id()
		if main.local_player and main.player_list.players.has(local_pid):
			main.player_list.players[local_pid].ping = ping
			main.player_list.players[local_pid].packet_loss = packet_loss
			main.push_local_player_info()


	text += "\nFPS: " + str(Engine.get_frames_per_second())
	if Engine.target_fps != 0:
		text += " (capped at " + str(Engine.target_fps) + ")"
	text += "\n" + main.version + " · [color=#8888ff][b][url=https://libla.st]libla.st[/url][/b][/color]"

func _on_performance_meta_clicked(meta: String):
	# meta: Link to the project which is placed within the url tag
	# Opens the link in the default browser on click
	OS.shell_open(meta)
