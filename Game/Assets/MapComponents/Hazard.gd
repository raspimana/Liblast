extends Area3D

func _on_Hazard_body_entered(body) -> void: # Player did fall down
	body.rpc(&'die', body.get_multiplayer_authority())
